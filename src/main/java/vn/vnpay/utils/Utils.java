/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.vnpay.utils;

import com.google.gson.Gson;
import java.security.MessageDigest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.vnpay.bean.RequestMerchant;

/**
 *
 * @author hungnv1
 */
public class Utils {

    private static final Logger LOGGER = LogManager.getLogger(Utils.class);
    public static final String EMPTY = "";
    public static boolean isEmpty(String value) {
        return (value == null) || (value.trim().isEmpty());
    }

    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }

    public static String makeChecksum(RequestMerchant request, String secretKey, String tokenKey) {
        String data = request.getCode() + "|" + request.getMsgType() + "|" + request.getTxnId() + "|" + request.getQrTrace() + "|" + request.getBankCode() + "|" + request.getMobile() + "|" + request.getAccountNo() + "|" + request.getAmount() + "|" + request.getPayDate() + "|" + request.getMerchantCode() + "|" + secretKey;

        LOGGER.info("Token [{}] - Data: {} before checkSum.", new Object[]{tokenKey, data});
        return encodeMD5LowCase(data);
    }

    public static String encodeMD5LowCase(String s) {
        if (isEmpty(s)) {
            return "";
        }
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        try {
            byte[] strTemp = s.getBytes();
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(strTemp);
            byte[] md = messageDigest.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[(k++)] = hexDigits[(byte0 >>> 4 & 0xF)];
                str[(k++)] = hexDigits[(byte0 & 0xF)];
            }
            return String.valueOf(str);
        } catch (Exception e) {
        }
        return "";
    }
}
