package vn.vnpay.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

@ToString
@Builder
@Getter
@Setter
public class Data {

    private String txnId;
}
