package vn.vnpay.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

@ToString
@Builder
@Getter
@Setter
public class QrCodeItemPayment {

    private String merchantType;
    private String serviceCode;
    private String masterMerCode;
    private String merchantCode;
    private String terminalId;
    private String productId;
    private String amount;
    private String tipAndFee;
    private String ccy;
    private String qty;
    private String note;

}
