package vn.vnpay.bean;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

@ToString
@Builder
@Getter
@Setter
public class RequestMerchant {

    private String code;
    private String message;
    private String msgType;
    private String txnId;
    private String qrTrace;
    private String bankCode;
    private String mobile;
    private String accountNo;
    private String amount;
    private String payDate;
    private String masterMerCode;
    private String merchantCode;
    private String terminalId;
    private List<QrCodeItemPayment> addData;
    private String checksum;
    private String urlMerchant;
    private String ccy;
    private String secretKey;

}
